/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codeformat;

import java.io.*;
import java.util.EmptyStackException;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nick
 */
public class CodeFormat {

    public static void main(String[] args) {
        try {
            format(args[0], args[1]);
        } catch (IOException ex) {
            Logger.getLogger(CodeFormat.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void format(String inFileName, String outFileName) throws IOException {
        File sourceFile = new File(inFileName);
        if (!sourceFile.exists()) {
            throw new FileNotFoundException("File not found!");
        }
        FileInputStream in = new FileInputStream(sourceFile);
        int nextChar = in.read();
        int level = 0;
        StringBuilder newString = new StringBuilder();
        boolean wasSpace = false;
        
        File resultFile = new File(outFileName);
        FileOutputStream out = new FileOutputStream(resultFile);

        while (nextChar != -1) {
            nextChar = in.read();
            char c = (char) nextChar;
            if (c == ' ' && (wasSpace || newString.length() == level)
                    || c == '\n') {
                continue;
            }
            switch (c) {
                case ';':
                    newString.append(";\n");
                    out.write(newString.toString().getBytes());
                    //out newString!!!!
                    newString = tabbing(level);
                    break;
                case '{':
                    newString.append("{\n");
                    out.write(newString.toString().getBytes());
                    //out!!!
                    level++;
                    newString = tabbing(level);
                    break;
                case '}':
                    if (level == 0) {
                        newString = new StringBuilder("<Syntax error>");
                    } else {
                        level--;
                        newString = tabbing(level);
                    }
                    newString.append("}\n");
                    out.write(newString.toString().getBytes());
                    newString = tabbing(level);
                    break;
                default:
                    wasSpace = (c == ' ');
                    newString.append(c);

            }
        }
    }

    private static StringBuilder tabbing(int level) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < level; i++) {
            result.append('\t');
        }
        return result;
    }

}
